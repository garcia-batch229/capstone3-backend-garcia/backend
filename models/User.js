const mongoose = require("mongoose");


const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required!"]
	},
	mobileNo: {
		type: Number,
		required: false
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	registerDate: {
		type: Date,
		default: new Date()
	},
	orders: [{
		productId: {
			type: String,
			required: [true, "Product ID is requried!"]
		},
		totalAmount: {
			type: String,
			required: [true, "Amount is required!"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}]
		
})




module.exports = mongoose.model("User", userSchema);